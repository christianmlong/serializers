"""Module providing a pretty-printer for JSON cassette_data.

This serializer does not save the timestamp data in the recorded_at attribute.
That way, you can re-record the cassette and not have a bunch of meaningless
diffs because the recorded_at date changed.

:author: Christian Long
"""
import json
import copy

from betamax.serializers import JSONSerializer


class PrettyJSONWithoutRecordedAtSerializer(JSONSerializer):
    name = 'prettyjson_without_recorded_at'

    def serialize(self, cassette_data):

        # Make our own copy of the dict, and replace recording timestamp with
        # an empty string.
        cassette_data_copy = copy.deepcopy(cassette_data)
        try:
            cassette_data_copy['http_interactions'][0]['recorded_at'] = ''
        except KeyError:
            pass

        return json.dumps(
            cassette_data_copy,
            sort_keys=True,
            indent=2,
            separators=(',', ': '),
        )
