Development Lead
----------------

- Ian Cordasco <graffatcolmingov@gmail.com>

Contributors
------------

PrettyJSONSerializer
````````````````````

- Jeremy Thurgood, original author

PrettyJSONWithoutRecordedAtSerializer
````````````````````

- Christian Long <christianzlong2@gmail.com>, original author
